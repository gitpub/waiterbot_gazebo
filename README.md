## How to start the project

Run in four separate shells and IN ORDER
```bash
# start gazebo
roslaunch waiterbot_gazebo waiterbot.launch

# start services
roslaunch waiterbot_gazebo services.launch

# start rviz
roslaunch waiterbot_gazebo navigation.launch

# start the simulation
rostopic pub /waiterbot/start_simulation std_msgs/String start
```


#### Other usefull commands

```bash
roslaunch turtlebot3_slam turtlebot3_slam.launch slam_methods:=gmapping
rosrun map_server map_saver -f ~/sdr/map

roslaunch turtlebot3_teleop turtlebot3_teleop_key.launch
```

#### Debug

```bash
rosrun tf2_tools view_frames.py
rosrun rqt_graph rqt_graph
```
